import Header from '../header/header';

const Main = () => {
  return (
    <main className="main">
      <Header/>
      <div className="main__wrapper">
        <h2 className="main__title">Что в мире творится?</h2>
        <p className="main__text">Введите в поиске любую тему и узнайте, насколько популярной она была в
          новостях за прошедшую неделю.
        </p>
        <form className="search" noValidate>
          <input id="search" type="text" className="search__input" placeholder="Введите тему новости" required/>
            <span id="search-error" className="search__error"></span>
            <button className="button button_styles_search" type="submit" disabled>Искать</button>
        </form>
      </div>
    </main>
  )
}

export default Main;