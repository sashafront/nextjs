import Link from 'next/link';

const Header = () => {
  return (
    <header className="header">
      <div className="header__wrapper">
        <h1 className="header__title">
          <Link href="/">
            <a className="header__title-link link-animation">NewsAnalyzer</a>
          </Link>
        </h1>
        <nav className="header__menu">
          <ul className="header__list">
            <li className="header__item">
              <Link href="/">
                <a className="header__link link-animation header__link_active_index">Главная</a>
              </Link>
            </li>
            <li className="header__item">
              <Link href="/about">
                <a className="header__link link-animation">О проекте</a>
              </Link>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Header;